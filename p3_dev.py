"""MATH 96012 Project 3
Contains four functions:
    simulate2: Simulate bacterial dynamics over m trials. Return: all positions at final time
        and alpha at nt+1 times averaged across the m trials.
    performance: To be completed -- analyze and assess performance of python, fortran, and fortran+openmp simulation codes
    correlation: To be completed -- compute and analyze correlation function, C(tau)
    visualize: To be completed -- generate animation illustrating "non-trivial" particle dynamics
"""
import numpy as np
import matplotlib.pyplot as plt
from m1 import bmotion as bm #assumes that p3_dev.f90 has been compiled with: f2py --f90flags='-fopenmp' -c p3_dev.f90 -m m1 -lgomp
import scipy.spatial.distance as scd
import time
#May also use scipy and time modules as needed
tau = np.linspace(0,81,82)

def simulate2(M,L,s0,r0,A,Nt,N):
    """Simulate bacterial colony dynamics
    Input:
    M: Number of simulations
    N: number of particles
    L: length of side of square domain
    s0: speed of particles
    r0: particles within distance r0 of particle i influence direction of motion
    of particle i
    A: amplitude of noise
    Nt: number of time steps

    Output:
    X,Y: position of all N particles at Nt+1 times
    alpha: alignment parameter at Nt+1 times averaged across M simulation

    Do not modify input or return statement without instructor's permission.

    Add brief description of approach of differences from simulate1 here:
    This code carries out M simulations at a time with partial vectorization
    across the M samples.
    """
    #Set initial condition
    phi_init = np.random.rand(M,N)*(2*np.pi)
    r_init = np.sqrt(np.random.rand(M,N))
    Xinit,Yinit = r_init*np.cos(phi_init),r_init*np.sin(phi_init)
    Xinit+=L/2
    Yinit+=L/2
    #---------------------

    #Initialize variables
    P = np.zeros((M,N,2)) #positions
    P[:,:,0],P[:,:,1] = Xinit,Yinit
    alpha = np.zeros((M,Nt+1)) #alignment parameter
    S = np.zeros((M,N),dtype=complex) #phases
    T = np.random.rand(M,N)*(2*np.pi) #direction of motion
    n = np.zeros((M,N)) #number of neighbors
    E = np.zeros((M,N,Nt+1),dtype=complex)
    d = np.zeros((M,N,N))
    dtemp = np.zeros((M,N*(N-1)//2))
    AexpR = np.random.rand(M,N,Nt)*(2*np.pi)
    AexpR = A*np.exp(1j*AexpR)

    r0sq = r0**2
    E[:,:,0] = np.exp(1j*T)

    #Time marching-----------
    for i in range(Nt):
        for j in range(M):
            dtemp[j,:] = scd.pdist(P[j,:,:],metric='sqeuclidean')

        dtemp2 = dtemp<=r0sq
        for j in range(M):
            d[j,:,:] = scd.squareform(dtemp2[j,:])
        n = d.sum(axis=2) + 1
        S = E[:,:,i] + n*AexpR[:,:,i]

        for j in range(M):
            S[j,:] += d[j,:,:].dot(E[j,:,i])

        T = np.angle(S)

        #Update X,Y
        P[:,:,0] = P[:,:,0] + s0*np.cos(T)
        P[:,:,1] = P[:,:,1] + s0*np.sin(T)

        #Enforce periodic boundary conditions
        P = P%L

        E[:,:,i+1] = np.exp(1j*T)
    #----------------------

    #Compute order parameter
    alpha = (1/(N*M))*np.sum(np.abs(E.sum(axis=1)),axis=0)

    return P[:,:,0],P[:,:,1],alpha


def performance(N,display=False):
    """Assess performance of simulate2, simulate2_f90, and simulate2_omp
    Modify the contents of the tuple, input, as needed
    When display is True, figures equivalent to those
    you are submitting should be displayed
    """
    Time=np.zeros(40)
    N_vals=np.arange(100,500,10)
    m_vals=np.arange(100,500,10)
    bm.bm_l=8
    bm.bm_s0=0.2
    bm.bm_r0=1,
    bm.bm_a=0.64
    Time=np.zeros(40)
    Time2=np.zeros(40)  
    Time3=np.zeros(40)   
    for j in range(1,41):
	    
	    t0=time.time()
	    P,Q,alpha=simulate2(M=100,L=8,s0=0.2,r0=1,A=0.64,Nt=100,N=N_vals[j-1])
	    t1=time.time()
	    Time[j-1]=t1-t0
		    
	    t2=time.time()
	    x,y,alpha_ave=bm.simulate2_f90(m=100,n=N_vals[j-1],nt=100)
	    t3=time.time()
	    Time2[j-1]=t3-t2
    
	    t4=time.time()
	    x2,y2,alpha_ave2=bm.simulate2_omp(m=50,n=N_vals[j-1],nt=100)
	    t5=time.time()
	    Time3[j-1]=(t5-t4)
	    #print(t5-t4)


    fig = plt.figure()
    ax = plt.subplot(111)
    #plot of the simulate2 in Python versus N
    ax.plot(N_vals,Time, label='python')
    #plot of the simulate2 in fortran versus N
    ax.plot(N_vals, Time2, label='fortran')
    #plot of the parallelized version of simulate2_f90 versus N
    ax.plot(N_vals, Time3, label='fortran_omp')
    plt.title('Fortran and Fortran_omp time')
    ax.legend()
    plt.show()
    
    #speed-up
    fig2 = plt.figure()
    ax = plt.subplot(111)
    ax.plot(N_vals,Time2/Time3,label = 'speed-up')
    plt.title('speed-up')
    ax.legend()
    plt.show()

    for i in range(1,21):
    	 
	    t0=time.time()
	    P,Q,alpha=simulate2(M=m_vals[i-1],L=8,s0=0.2,r0=1,A=0.64,Nt=100,N=200)
	    t1=time.time()
	    Time[j-1]=t1-t0
	    t2=time.time()
	    x,y,alpha_ave=bm.simulate2_f90(m=m_vals[i-1],n=200,nt=100)
	    t3=time.time()
	    Time2[j-1]=t3-t2
    
	    t4=time.time()
	    x2,y2,alpha_ave2=bm.simulate2_omp(m=m_vals[i-1],n=200,nt=100)
	    t5=time.time()
	    Time3[j-1]=(t5-t4)
	    #print(t5-t4)       
    
    fig3 = plt.figure()
    ax = plt.subplot(111)
    #plot of the simulate2 in Python versus m
    ax.plot(m_vals,Time, label='python')
    #plot of the simulate2 in fortran versus m
    ax.plot(m_vals, Time2, label='fortran')
    #plot of the parallelized version of simulate2_f90 versus m
    ax.plot(m_vals, Time3, label='fortran_omp')
    plt.title('Time taken by Python and Fortran simulate2 when m is varried')
    ax.legend()
    plt.show()
    
    return None #Modify as needed

def correlation(tau,display=False):
    """Compute and analyze temporal correlation function, C(tau)
    Modify the contents of the tuple, input, as needed
    When display is True, figures equivalent to those
    you are submitting should be displayed"""
    
    x,y,alpha=simulate2(M=100,L=16,s0=0.1,r0=1,A=0.625,Nt=400,N=400)
    #initialize a tau to take all integers between 0 and 80
    tau = np.linspace(0,81,82)
    C=np.zeros(82)
    for j in range(82):
    	
    	#Calculate the correlation for tau=j
    	C[j]=(1/200)*np.dot(alpha[100:300],alpha[100+j:300+j])-np.mean(alpha[100:300])**2
    
    #plot of the correlation in function of the value of Tau
    fig=plt.figure()
    ax = plt.subplot(111)
    ax.plot(tau,C,'.',label='correlation versus tau')
    ax.legend()
    plt.title('correlation when tau varies')
    plt.show()
    return None #Modify as needed



def visualize(Display=True):
    """Generate an animation illustrating the evolution of
        villages during C vs M competition
    """
    import matplotlib.animation as animation
    bm.bm_l=8
    bm.bm_s0=0.2
    bm.bm_r0=1,
    bm.bm_a=0.2
    x,y=bm.simulate2_modified(n=128,nt=200)
    fig, ax = plt.subplots()
    line, = ax.plot(x[:,0],y[:,0],'.')
    ax.set_xlim(0,bm.bm_l)
    ax.set_ylim(0,bm.bm_l)
 
    def updatefig(i):	
    	print("iteration=",i)
    	line.set_ydata(y[:,i])
    	line.set_xdata(x[:,i])
    	return line,
    
    ani = animation.FuncAnimation(fig, updatefig, frames=100,interval=350,blit=True,repeat=True)
    #plt.show()
    ani.save('question4.mp4', writer = "ffmpeg")
    fig.show()
    return None #Modify as needed
    

if __name__ == '__main__':
    #Modify the code here so that it calls performance analyze and
    # generates the figures that you are submitting with your code

    input_p = []
    output_p=input_p.append(performance(input_p,display=True)) #modify as needed

    input_c = []
    output_c = input_c.append(correlation(input_c))

    input_a = []
    output_a = input_a.append(visualize(input_a))
    
