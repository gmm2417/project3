!MATH 96012 Project 3
!This module contains four module variables and two subroutines;
!one of these routines must be developed for this assignment.
!Module variables--
! bm_l, bm_s0, bm_r0, bm_a: the parameters l, s0, r0, and a in the particle dynamics model
! numthreads: The number of threads that should be used in parallel regions within simulate2_omp
!
!Module routines---
! simulate2_f90: Simulate particle dynamics over m trials. Return: all x-y positions at final time
! and alpha at nt+1 times averaged across the m trials.
! simulate2_omp: Same input/output functionality as simulate2.f90 but parallelized with OpenMP

module bmotion
  use omp_lib
  implicit none
  integer :: numthreads
  real(kind=8) :: bm_l,bm_s0,bm_r0,bm_a
  real(kind=8), parameter :: pi = acos(-1.d0)
  complex(kind=8), parameter :: ii = complex(0.d0,1.d0)

contains

!Compute m particle dynamics simulations using the parameters,bm_l,bm_s0,bm_r0,bm_a.
!Input:
!m: number of simulations
!n: number of particles
!nt: number of time steps
!Output:
! x: x-positions at final time step for all m trials
! y: y-positions at final time step for all m trials
! alpha_ave: alignment parameter at each time step (including initial condition)
! averaged across the m trials
subroutine simulate2_f90(m,n,nt,x,y,alpha_ave)
  implicit none
  integer, intent(in) :: m,n,nt
  real(kind=8), dimension(m,n), intent(out) :: x,y
  real(kind=8), dimension(nt+1), intent(out) :: alpha_ave
  integer :: i1,j1,k1
  real(kind=8), dimension(m,n) :: nn !neighbors
  real(kind=8) :: r0sq !r0^2
  real(kind=8), dimension(m,n) :: phi_init,r_init,theta !used for initial conditions
  real(kind=8), dimension(m,n,n) :: dist2 !distance squared
  real(kind=8), allocatable, dimension(:,:,:) :: temp
  complex(kind=8), dimension(m,n) :: phase
  complex(kind=8), dimension(m,n,nt+1) :: exp_theta,AexpR


!---Set initial condition and initialize variables----
  allocate(temp(m,n,nt+1))
  call random_number(phi_init)
  call random_number(r_init)
  call random_number(theta)
  call random_number(temp)

  phi_init = phi_init*(2.d0*pi)
  r_init = sqrt(r_init)
  theta = theta*(2.d0*pi) !initial direction of motion
  x = r_init*cos(phi_init)+0.5d0*bm_l !initial positions
  y = r_init*sin(phi_init)+0.5d0*bm_l

  alpha_ave=0.d0
  r0sq = bm_r0*bm_r0
  exp_theta(:,:,1) = exp(ii*theta)
  AexpR = bm_a*exp(ii*temp*2.d0*pi) !noise term
  deallocate(temp)
  nn = 0

!----------------------------------------------
  !Time marching
  do i1 = 2,nt+1

    phase=0.d0

    !Compute distances
    do j1=1,n-1
      do k1 = j1+1,n
        dist2(:,j1,k1) = (x(:,j1)-x(:,k1))**2 + (y(:,j1)-y(:,k1))**2
        where (dist2(:,j1,k1)>r0sq)
          dist2(:,j1,k1)=0
        elsewhere
          dist2(:,j1,k1)=1
        end where
       dist2(:,k1,j1) =dist2(:,j1,k1)
      end do
    end do

    nn = sum(dist2,dim=3)+1

    !Update phase
    phase =  exp_theta(:,:,i1-1) +nn*AexpR(:,:,i1-1)
    do j1=1,m
      phase(j1,:) = phase(j1,:) + matmul(dist2(j1,:,:),exp_theta(j1,:,i1-1))
    end do

    !Update Theta
    theta = atan2(aimag(phase),real(phase))

    !Update X,Y
    x = x + bm_s0*cos(theta)
    y = y + bm_s0*sin(theta)

    x = modulo(x,bm_l)
    y = modulo(y,bm_l)

    exp_theta(:,:,i1) = exp(ii*theta)

  end do

  alpha_ave = (1.d0/dble(m*n))*sum(abs(sum(exp_theta,dim=2)),dim=1)

end subroutine simulate2_f90


!Same functionality as simulate2_f90, but parallelized with OpenMP
!Parallel regions should use numthreads threads.
!Compute m particle dynamics simulations using the parameters,bm_l,bm_s0,bm_r0,bm_a.
!Same functionality as simulate2_f90, but parallelized with OpenMP
!Parallel regions should use numthreads threads.
!Input:
!m: number of simulations
!n: number of particles
!nt: number of time steps
!Output:
! x: x-positions at final time step for all m trials
! y: y-positions at final time step for all m trials
! alpha_ave: alignment parameter at each time step (including initial condition)
! averaged across the m trials


!For the simulate2_omp version I did not pareallelized the outer loop on the time steps as the position we calculate depends on the position one time step before. However I transformed the vectorised part at the beginning into an other loop and I then collapsed the first two loops and parallelised them. I put k1 in private as it is dependant of j1. As I devectorised the beginning, I had to change the where part which works for arrays only intop an if then statement. For the phase update, as it is always a sum from the previous value, I parallelized this loop using reduction with the sum operation. Indeed as it is just of the form phase=phas+..., it is parallelizable if we put the reduction part. What I could have also done is to put the loop on the iterations ouside the loop on the time steps and then parallelized the outer loop without doing anything else. I tried doing this but got result pretty much similar in terms of speed up.



subroutine simulate2_omp(m,n,nt,x,y,alpha_ave)
  use omp_lib
  implicit none
  integer, intent(in) :: m,n,nt
  real(kind=8), dimension(m,n), intent(out) :: x,y
  real(kind=8), dimension(nt+1), intent(out) :: alpha_ave
  integer :: i1,j1,k1,k2,j2
  real(kind=8), dimension(m,n) :: nn !neighbors
  real(kind=8) :: r0sq !r0^2
  real(kind=8), dimension(m,n) :: phi_init,r_init,theta !used for initial conditions
  real(kind=8), dimension(m,n,n) :: dist2 !distance squared
  real(kind=8), allocatable, dimension(:,:,:) :: temp
  complex(kind=8), dimension(m,n) :: phase
  complex(kind=8), dimension(m,n,nt+1) :: exp_theta,AexpR

  !used for initial conditions
  !Note: use allocatable arrays where possible to avoid OpenMP memory issues

!---Set initial condition and initialize variables (does not need to be parallelized)----

allocate(temp(m,n,nt+1))
call random_number(phi_init)
call random_number(r_init)
call random_number(theta)
call random_number(temp)

phi_init = phi_init*(2.d0*pi)
r_init = sqrt(r_init)
theta = theta*(2.d0*pi) !initial direction of motion
x = r_init*cos(phi_init)+0.5d0*bm_l !initial positions
y = r_init*sin(phi_init)+0.5d0*bm_l

alpha_ave=0.d0
r0sq = bm_r0*bm_r0
exp_theta(:,:,1) = exp(ii*theta)
AexpR = bm_a*exp(ii*temp*2.d0*pi) !noise term
deallocate(temp)
nn = 0

!----------------------------------------------
!Time marching
!$ CALL omp_set_num_threads(numthreads)

do i1 = 2,nt+1
!Compute distances
!$OMP PARALLEL DO PRIVATE(k1) COLLAPSE(2)
do j1=1,n-1
  do k2=1,m
    do k1 = j1+1,n

    dist2(k2,j1,k1) = (x(k2,j1)-x(k2,k1))**2 + (y(k2,j1)-y(k2,k1))**2
    if (dist2(k2,j1,k1)>r0sq) then
      dist2(k2,j1,k1)=0
    else
      dist2(k2,j1,k1)=1
    end if
   dist2(k2,k1,j1) =dist2(k2,j1,k1)
    end do
  end do
end do
!$OMP END PARALLEL DO
phase=0.d0
nn = sum(dist2,dim=3)+1

!Update phase
phase =  exp_theta(:,:,i1-1) +nn*AexpR(:,:,i1-1)
!$OMP PARALLEL DO reduction(+:phase)
do j2=1,m
  phase(j2,:) = phase(j2,:) + matmul(dist2(j2,:,:),exp_theta(j2,:,i1-1))
end do
!$OMP END PARALLEL DO
!Update Theta
theta = atan2(aimag(phase),real(phase))

!Update X,Y
x = x + bm_s0*cos(theta)
y = y + bm_s0*sin(theta)

x = modulo(x,bm_l)
y = modulo(y,bm_l)

exp_theta(:,:,i1) = exp(ii*theta)

end do

alpha_ave = (1.d0/dble(m*n))*sum(abs(sum(exp_theta,dim=2)),dim=1)



end subroutine simulate2_omp



!For the question 4, I created this ;odified version of simulate2_f90 which only have 1 iteration (so I got rid of the m) and which returns all the positions x and y at all the time step from 0 to nt where 0 is the initial position. With this new function I was able to do an animation of the bateria using lab7 and I saved them as question4.mp4 using ffmpeg.

subroutine simulate2_modified(n,nt,x2,y2)
  implicit none
  integer, intent(in) :: n,nt
  real(kind=8), dimension(n,nt+1), intent(out) :: x2,y2
  integer :: i1,j1,k1
  real(kind=8), dimension(n) :: nn !neighbors
  real(kind=8) :: r0sq !r0^2
  real(kind=8), dimension(n) :: phi_init,r_init,theta !used for initial conditions
  real(kind=8), dimension(n,n) :: dist2 !distance squared
  real(kind=8), allocatable, dimension(:,:) :: temp
  complex(kind=8), dimension(n) :: phase
  complex(kind=8), dimension(n,nt+1) :: exp_theta,AexpR


!---Set initial condition and initialize variables----
  allocate(temp(n,nt+1))
  call random_number(phi_init)
  call random_number(r_init)
  call random_number(theta)
  call random_number(temp)

  phi_init = phi_init*(2.d0*pi)
  r_init = sqrt(r_init)
  theta = theta*(2.d0*pi) !initial direction of motion
  x2(:,1) = r_init*cos(phi_init)+0.5d0*bm_l !initial positions
  y2(:,1) = r_init*sin(phi_init)+0.5d0*bm_l

  
  r0sq = bm_r0*bm_r0
  exp_theta(:,1) = exp(ii*theta)
  AexpR = bm_a*exp(ii*temp*2.d0*pi) !noise term
  deallocate(temp)
  nn = 0

!----------------------------------------------
  !Time marching
  do i1 = 2,nt+1

    phase=0.d0

    !Compute distances
    do j1=1,n-1
      do k1 = j1+1,n
        dist2(j1,k1) = (x2(j1,i1-1)-x2(k1,i1-1))**2 + (y2(j1,i1-1)-y2(k1,i1-1))**2
        if (dist2(j1,k1)>r0sq) then
          dist2(j1,k1)=0
        else
          dist2(j1,k1)=1
        end if
       dist2(k1,j1) =dist2(j1,k1)
      end do
    end do

    nn = sum(dist2,dim=2)+1

    !Update phase
    phase =  exp_theta(:,i1-1) +nn*AexpR(:,i1-1)
  

    !Update Theta
    theta = atan2(aimag(phase),real(phase))

    !Update X,Y
    x2(:,i1) = x2(:,i1-1) + bm_s0*cos(theta)
    y2(:,i1) = y2(:,i1-1) + bm_s0*sin(theta)

    x2(:,i1) = modulo(x2(:,i1),bm_l)
    y2(:,i1) = modulo(y2(:,i1),bm_l)

    exp_theta(:,i1) = exp(ii*theta)

  end do


end subroutine simulate2_modified



end module bmotion
